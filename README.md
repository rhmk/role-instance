# Ansible Role: instance

This role will help to create and remove instances on several different providers. It can also be used to demonstrate multi cloud use cases.

**WARNING: Support for Red Hat Virtualization currently has no owner and will no longer be maintained and eventually be removed! If you're interested in keeping it alive, please volunteer as a maintainer.**

## How to install this role

The Ansible SSA collection containing this role is shipped with the Ansible SSA Execution Environment. For best results, it is recommended to use the Execution Environment to make sure all prerequisites are met.

## How to use this role

The role is optimized to be used from the [Playbook RHAAP](https://gitlab.com/ansible-ssa/playbook-rhaap) which will be called by a Job Template created by this role.

If you want to use it in your playbook:

```yaml
- name: deploy instance
  var:
    type: gcp
    instance_name: demo
    instance_flavor: n1-standard-1
    instance_disk_size: 20
  include_role:
    name: ansible.ssa.general.instance
```

or for vmware:

```yaml
- name: deploy instance
  var:
    type: vmware
    instance_name: demo
    instance_group: controller
    instance_parameters: "{{ controller_instance_parameters | default ({}) }}"
  include_role:
    name: ansible.ssa.general.instance
```

or for vmware (second example):

```yaml
- name: deploy instance
  var:
    type: vmware
    instance_name: demo
    instance_group: controller
    instance_parameters.vmware_instance_diskgb: '10'
    instance_parameters.vmware_instance_memmb: '8192'
    instance_parameters.vmware_instance_cpus: '2
  include_role:
    name: ansible.ssa.general.instance
```

To remove or delete a VM or instance, use the same role, but with the "remove" variables set to true.

```yaml
- name: Remove instance
  var:
    type: gcp
    instance_name: demo
    remove: true
  include_role:
    name: ansible.ssa.general.instance
```

Instead of using the role directly, consider using the [Ansible SSA Collection](https://gitlab.com/ansible-ssa/ansible-ssa-collection)

## Variables

Check the [defaults/main.yml](./defaults/main.yml) for details on variables used by this role.
